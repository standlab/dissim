# -*- coding: utf-8 -*-
"""
Created on Wed Sep 12 18:24:15 2018

@author: Artem Vesnin
"""

from collections import namedtuple
from functools import lru_cache
import functools

Point = namedtuple('Point', ['x', 'y'])

Corners = namedtuple('Corners', ['lb', 'lt', 'rt', 'rb'])

class FieldModel():
    """
    This model surves for Units placement on the field. It defines such 
    property as rotation and location. It has cartesian coordinate system, 
    besides each unit has its own. Transformatiom from 'global' coordinates
    to 'unit' coordinates is given for each unit as it is registered in field.
    The units strored as dictionary, keys are unit name. See register_unit()
    for list of unit properties stored.
    
    Units are placed by rotation and the position of the unit origin
    (left bottom corner of the unit as cartesian coordinate are defined).
    
    Units are know by means of its corners: left bottom, left top,
    right top and right bottom. This corners are defined as they are seen on
    the field. These corners are different from the unit corners for any 
    but 'up' rotation. See register_unit() for rotions. On the scheme bellow 
    the corners (on the field) and origin of the unit are given. The origin of 
    each unit is denoted as 'o' and is always lb corner in the unit 
    coordinates
    
   lt_ _ _ _ rt                    lt_ _ _ _ rt
    |       |    lt_ _ _ _ _ _ rt   |      o|     lt_ _ _ _ _ _ rt
    |       |     |o          |     |   D   |      |           |
    |   U   |     |           |     |   O   |      |           |
    |   P   |     |  RIGHT    |     |   W   |      |   LEFT    |
    |       |     |_ _ _ _ _ _|     |   N   |      |_ _ _ _ _ o| 
    |o _ _ _|    lb            rb   |_ _ _ _|     lb            rb
   lb        rb                    lb        rb
   
    The field defines unit placement, unit realtion and coordinate 
    transformation. The cells and zones are still stored in Unit.
    """
    def __init__(self, units = {}):
        self.units = units
        self.xlimits = (0, 0)
        self.ylimits = (0, 0)
        
    @property    
    def x_min(self):
        return self.xlimits[0]

    @property    
    def x_max(self):
        return self.xlimits[1]

    @property    
    def y_min(self):
        return self.ylimits[0]

    @property    
    def y_max(self):
        return self.ylimits[1]

    def _update_xlimits(self, xmin, xmax):
        self.xlimits = (min(self.x_min, xmin), max(self.x_max, xmax))
        
    def _update_ylimits(self, ymin, ymax):
        self.ylimits = (min(self.y_min, ymin), max(self.y_max, ymax))
        
    #@lru_cache(maxsize=32)
    def _get_corners(self, unit, x, y, rotation):
        corners = {}
        if rotation == 'up':
            corners = Corners(Point(x, y), 
                              Point(x, y + unit.rows - 1), 
                              Point(x + unit.cols - 1, y + unit.rows - 1), 
                              Point(x + unit.cols - 1, y))
        elif rotation == 'right':
            corners = Corners(Point(x, y - (unit.cols - 1)), 
                              Point(x, y), 
                              Point(x + unit.rows - 1, y), 
                              Point(x + unit.rows - 1, y - (unit.cols - 1)))
        elif rotation == 'down':
            corners = Corners(Point(x - (unit.cols - 1), y - (unit.rows - 1)), 
                              Point(x - (unit.cols - 1), y), 
                              Point(x, y), 
                              Point(x, y - (unit.rows - 1)))
        elif rotation == 'left':
            corners = Corners(Point(x - (unit.rows - 1), y), 
                              Point(x - (unit.rows - 1), y + unit.cols - 1), 
                              Point(x, y + unit.cols - 1), 
                              Point(x, y))
        else:
            raise ValueError('Unknown rotation {} '.format(rotation))
        return corners
        
        
    def _update_limits(self, unit, x, y, rotation):
        """
        Updates x and y extents of the field. They could be nagative, the 
        View should take it into account.
        """
        corners = self._get_corners(unit, x, y, rotation)
        self._update_xlimits(corners.lb.x, corners.rt.x)
        self._update_ylimits(corners.lb.y, corners.rt.y)
        
    def _in_unit(self, x, y):
        """
        Return the name of the unit (x,y) of the field are belong to.
        """
        name = None
        for u_name, u in self.units.items():
            u_corners = self._get_corners(u['unit'], u['x'], u['y'], 
                                          u['rotation'])
            if (u_corners.lb.x <= x <= u_corners.rt.x and
                u_corners.lb.y <= y <= u_corners.rt.y):
                name = u_name
                break
        return name
            
        
    def _check_unit_conflict(self, unit, x, y, rotation):
        """
        Check if unit conflicts with already registered units.
        
        Return True is check is OK
        """
        if not self.units:
            return True
        corners = self._get_corners(unit, x, y, rotation)
        for corner in corners:
            if self._in_unit(x, y) is not None:
                return False
        return True
    
    def _transform_up(self, x, y, xo, yo):
        return x - xo, y - yo
    
    def _transform_right(self, x, y, xo, yo):
        return yo - y, x - xo

    def _transform_down(self, x, y, xo, yo):
        return xo - x, yo - y

    def _transform_left(self, x, y, xo, yo):
        return y - yo, xo - x
        
    def register_unit(self, unit_name, unit, x, y, rotation):
        """
        Registers one unit in field. New unit must not instersect existing 
        ones.
        
        :param unit_name: string 
        The name unit is known in field
        
        :param unit: UnitModel
        
        :param x,y: int
        
        x and y coordinates of the unit origin (left bottom corner)
        
        :param rotation: string
            
        rotation of unit on the field, there are four possible rotations
        up, right, down, left
                             _ _ _ _
                            |       |
                 _ _ _ _ _ _|       |
                |           |   U   |
                |    LEFT   |   P   |
                |           |       |
                |_ _ _ _ _ o|o _ _ _|_ _
                    |      o|o          |
                    |   D   |   RIGHT   |
                    |   O   |           |
                    |   W   |_ _ _ _ _ _|
                    |   N   |
                    |_ _ _ _|
        """
        if unit_name in self.units:
            raise ValueError('Unit {} already exists'.format(unit_name))
        if not self._check_unit_conflict(unit, x, y, rotation):
            raise ValueError('{} has conlict with others'.format(unit_name))
        self._update_limits(unit, x, y, rotation)
        if rotation == 'up':
            t = functools.partial(self._transform_up, xo=x, yo=y)
        elif rotation == 'right':
            t = functools.partial(self._transform_right, xo=x, yo=y)
        elif rotation == 'down':
            t = functools.partial(self._transform_down, xo=x, yo=y)
        elif rotation == 'left':
            t = functools.partial(self._transform_left, xo=x, yo=y)
        corners = self._get_corners(unit, x, y, rotation)
        self.units[unit_name] = {'unit': unit, 
                                 'rotation': rotation, 
                                 'x': x, 'y': y,
                                 'transform': t,
                                 'corners': corners}
                                 
    def get_cell(self, x, y):
        """
        Returns the cell at (x,y) on the field. The coordiate transform is 
        made and then propert unit is requected for cell.
        :param x,y: int
        Coordinate of requested cell on the field.
        """
        u_name = self._in_unit(x, y)
        if u_name is None:
            return None
        unit = self.units[u_name]   
        x_unit, y_unit = unit['transform'](x, y)
        return unit['unit'].get_cell_in_unit(x_unit, y_unit)
        
        
    def get_state(self):
        """
        Return entire state of the field as list of list. 
        """
        state = []
        for x in range(self.x_min, self.x_max + 1):
            state.append(list())
            for y in range(self.y_min, self.y_max + 1):
                cell = self.get_cell(x, y)
                if cell is None:
                    state[-1].append('None')
                else:
                    state[-1].append(cell.info)
        return {'field': state}
        


class UnitModel():
    """
    The model is for one Unit. The Unit could be consider as one physical
    device simulator is written for. The unit consists of cells and is 
    virtually marked by zones. 
    Cells are stored in self.zones. Cells are prescribed to zone and are given 
    by id retrieved from x and y coordinates on the unit (not zone). The ids 
    go through the unit.
    """
    def __init__(self, dims=(10, 15), sol_pth=None, **kwargs):
        self.sol_pth = sol_pth
        self.dims = dims[:]
        self.default_color = kwargs.get('color', 'rgb(128, 128, 100)')
        self.controls = []
        self.zones_prop = None
        self.zones = None
        self.define_zones()
        

    @property
    def rows(self):
        return self.dims[1]

    @property
    def cols(self):
        return self.dims[0]

    @property
    def default_zone_prop(self):
        return {'main': {'x_bottom_left': 0,
                         'y_bottom_left': 0,
                         'x_top_right': self.dims[1] - 1,
                         'y_top_right': self.dims[0] - 1,
                         'color': self.default_color}}

    def define_zones(self, zones_prop=None):
        """
        Sets the zone properties.
        :param zone_prop: dict of dict
        Zones given by names (keys of the first dict), the keys of the nested 
        dicts are the properties name.
        """
        if zones_prop is None:
            self.zones_prop = self.default_zone_prop
        else:
            self.zones_prop = zones_prop
        self._check_zones()
        self._create_zones()
        
    def _cell_id(self, x, y):
        return 'x' + str(x) + 'y' + str(y)

    def _cell_id_to_xy(self, cell_id):
        _id = cell_id[1:]
        tokens = _id.split('y')
        return (int(tokens[0]), int(tokens[1]))

    def _create_zones(self):
        """
        Marks the unit with the zones given by it properies see 
        default_zone_prop() for property list. Zones are given by left bottom
        and top right, those angles are both included, so to address all cells
        use:
            
        range(props['x_bottom_left'], props['x_top_right'] + 1)
        range(props['y_bottom_left'], props['y_top_right'] + 1)
        
        """
        if self.zones_prop is None:
            raise ValueError('Define zones poperties first.')
        self.zones = {}
        for zone_name, props in self.zones_prop.items():
            zone = {}
            y_range = range(props['x_bottom_left'], props['x_top_right'] + 1)
            x_range = range(props['x_bottom_left'], props['y_top_right'] + 1)
            for row in y_range:
                for col in x_range:
                    key = self._cell_id(col, row)
                    zone[key] = Cell(zone_name + '_cell',
                                     color=props['color'])
            self.zones[zone_name] = zone

    def _check_zones(self):
        """
        Check if unit is entirely marked by zones and zones do not interfere
        with each other.
        """
        area = 0
        for _, zone in self.zones_prop.items():
            area += ((zone['x_top_right'] - zone['x_bottom_left'] + 1) *
                     (zone['y_top_right'] - zone['y_bottom_left'] + 1))
        if area != self.dims[0] * self.dims[1]:
            msg = 'Total area {} is not equal zones area {}'
            msg = msg.format(self.dims[0] * self.dims[1], area)
            raise ValueError(msg)

    def is_inside_zone(self, x, y, zone_name, absolute_indexing=False):
        """
        Return True if (x,y) is inside zone given by zone_name. 
        
        :param x, y: int 
        Coordinates where the cell is stored
        
        :param zone_name: string
        
        :param absolute_indexing: boolean
        If True (x,y) are considered as coordinates of the unit, if False
        (x,y) are the coordinates relative zone left bottm corner.
        """
        assert zone_name in self.zones
        zone = self.zones_prop[zone_name]
        if absolute_indexing:
            return (zone['x_bottom_left'] < x < zone['x_top_right'] and
                    zone['y_bottom_left'] < y < zone['y_top_right'])
        else:
            return (0 < x < zone['x_top_right'] - zone['x_bottom_left'] and
                    0 < y < zone['y_top_right'] - zone['y_bottom_left'])

    def register_cell(self, cell, zone_name, x, y):
        """
        Register a cell in (x,y) coordinates. zone_name is given for reference
        if an user gives the x,y belonging to another than 'zone_name' the 
        cell will be stored in zone and warning will thrown
        
        :param cell: Cell
        A cell to be registered
        
        :param x, y: int 
        Coordinates where the cell is stored
        
        :param zone_name: string
        """
        assert zone_name in self.zones
        if not self.is_inside_zone(x, y, zone_name):
            msg = "WARNING: cell is not register since x={} and y={} "
            msg += "are out of zone {}"
            msg = msg.format(x, y, zone_name)
            print(msg)
            for zone in self.zones:
                if self.is_inside_zone(x, y, zone):
                    zone_name = zone
        self.zones[zone_name][self._cell_id(x, y)] = cell


    def get_cell(self, zone_name, x, y, absolute_indexing=False):
        """
        Returns cell given by (x,y) from zone given by zone_name. If (x,y) 
        is not in zone_name the exception will be thrown. 
        
        :param x, y: int 
        Coordinates where the cell is stored
        
        :param zone_name: string
        
        :param absolute_indexing: boolean
        If True (x,y) are considered as coordinates of the unit, if False
        (x,y) are the coordinates relative zone left bottm corner.
        """
        zone_prop = self.zones_prop[zone_name]
        if absolute_indexing:
            x = x - zone_prop['x_bottom_left']
            y = y - zone_prop['y_bottom_left']
        if self.is_inside_zone(x, y, zone_name, absolute_indexing):
            return self.zones[zone_name][self._cell_id(x, y)]
        else:
            raise ValueError('Out of range x = {} and  y = {}'.format(x, y))

    def get_cell_in_unit(self, x, y):
        """
        Returns cell given by (x,y) coordinates in the unit coordinate system. 
        It iterates the zones and check if cell is inside the zone and returns 
        it.
        """
        cell_id = self._cell_id(x, y)
        cell = None
        for _, cells in self.zones.items():
            if cell_id in cells:
                cell = cells[cell_id]
                break
        return cell
            
    def register_control(self, control):
        self.controls.append(control)


class Cell():

    def __init__(self, name, **kwargs):
        self.name = name
        self.id = name
        self.props = kwargs
        self.zones = []

    def in_zones(self, zone_names):
        """
        Defines which zones cell can belong to.
        """
        for name in zone_names:
            self.zones.append(name)

    @property
    def info(self):
        return self.name
