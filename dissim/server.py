# -*- coding: utf-8 -*-
"""
@author: Artem Vesnin
"""
from flask import Flask
import json

_app = Flask('dissim')

_dissim_model = None

def set_field(field):
    global _dissim_model 
    _dissim_model = field
    
@_app.route('/')
def hello():
    return 'This Discrete Simulator server based on flask.'

@_app.route('/state')
def state():
    return json.dumps(_dissim_model.get_state()).encode()

def run(url = '0.0.0.0', port = 5000):
    _app.run(host=url, port=5000)

