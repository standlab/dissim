"""
Created on Fri Aug 24 14:11:00 2018

@author: Artem Vesnin
"""

import json
import time
import os

from PyQt5.QtWidgets import QLabel, QWidget, QGridLayout
from PyQt5.QtWidgets import QSizePolicy
from PyQt5.QtCore import Qt, QThread
from PyQt5.QtGui import QPixmap


class Field(QWidget):
    
    def __init__(self, model, *args, **kwargs):
        QWidget.__init__(self, *args, **kwargs)
        self.model = model
        self.cell_width = kwargs.get('cell_width', 25)
        self.cell_height = kwargs.get('cell_height', 20)
        self.layout = self._create_layout()
        self._create_labels()
        self.show()
        
    def _create_layout(self):
        layout = QGridLayout()
        self.setLayout(layout)
        self.resize((self.model.x_max - self.model.x_min) * self.cell_width, 
                    (self.model.y_max - self.model.y_min) * self.cell_height)
        return layout
        
    def _create_labels(self):
        for x in range(self.model.x_min, self.model.x_max + 1):
            for y in range(self.model.y_min, self.model.y_max + 1):
                cell = self.model.get_cell(x, y)
                lbl_x = x - self.model.x_min
                lbl_y = (self.model.y_max - self.model.y_min) - (y - self.model.y_min)
                if cell is None:
                    self.layout.addWidget(BlankLabel(), lbl_y, lbl_x)
                else:   
                    lbl = CellLabel(cell, 
                                    width = self.cell_width,
                                    height = self.cell_height)
                    self.layout.addWidget(lbl, lbl_y, lbl_x)

class DumpThread(QThread):

    def __init__(self, simulator):
        super().__init__()
        self.simulator = simulator

    def run(self):
        if self.simulator.model.sol_pth is None:
            msg = 'ERROR: define pth where simultor model state is strored'
            raise ValueError(msg)
        while True:
            data = {}
            for zone_name, zone in self.simulator.model.zones.items():
                data[zone_name] = []
                for cell_id, cell in zone.items():
                    data[zone_name].append(cell.info)
            with open(self.simulator.model.sol_pth, 'w') as f:
                json.dump(data, f)
            time.sleep(5)


class CellLabel(QLabel):

    def __init__(self, cell, *args, **kwargs):
        super().__init__(*args)
        self.cell = cell
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.setAlignment(Qt.AlignCenter)
        style = 'QLabel {background-color: ' + cell.props['color'] + ';}'
        if cell.props.get('icon') is not None:
            img_pth = os.path.join(os.getcwd(), '../../') + cell.props['icon']
            icon = QPixmap(img_pth)
            self.setPixmap(icon.scaled(kwargs['height'], kwargs['width']))
        self.setStyleSheet(style)
        self.clicked = False

    def mousePressEvent(self, event):
        self.clicked = not self.clicked
        
        
class BlankLabel(QLabel):

    def __init__(self, *args):
        super().__init__(*args)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.setAlignment(Qt.AlignCenter)
        style = 'QLabel {background-color: rgb(255,255,255);}'
        self.setStyleSheet(style)
