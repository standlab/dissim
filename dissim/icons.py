# -*- coding: utf-8 -*-
"""
@author: Artem Vesnin
"""
from os import path

CREDIT_LYOLYA = 'Icon made by Lyolya from www.flaticon.com' 
FLATICON_URL = 'https://www.flaticon.com/free-icon'

ICONS = {}
ICONS['arrow_left'] = {'url': FLATICON_URL + '/left-arrow_109618'}
ICONS['arrow_right'] = {'url': FLATICON_URL + '/right-arrow_109617'}
ICONS['arrow_down'] = {'url': FLATICON_URL + '/download-arrow_109611'}
ICONS['arrow_up'] = {'url': FLATICON_URL + '/up-arrow_109583'}
ICONS['arrow_cw'] = {'url': FLATICON_URL + '/circular-arrow_109619'}
ICONS['arrow_ccw'] = {'url': FLATICON_URL + '/right-arrow_109614'}
ICONS['arrow_lb'] = {'url': FLATICON_URL + '/diagonal-arrow_109608'}
ICONS['arrow_lt'] = {'url': FLATICON_URL + '/diagonal-arrow_109603'}
ICONS['arrow_rb'] = {'url': FLATICON_URL + '/diagonal-arrow_109610'}
ICONS['arrow_rt'] = {'url': FLATICON_URL + '/diagonal-arrow_109607'}

for icon_name in ICONS:
    pth = path.join('img', path.join('icons',icon_name + '.png'))
    ICONS[icon_name]['path'] = pth
    ICONS[icon_name]['credit'] = CREDIT_LYOLYA