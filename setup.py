from setuptools import setup, find_packages
from codecs import open
from os import path

__version__ = ''
__author__ = 'Artem Vesnin'
__email__ = 'artemvesnin@gmail.com'


here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='dissim',

    description='',
    long_description=long_description,

    version=__version__,

    url='https://gitlab.com/standlab/dissim',

    author=__author__,
    author_email=__email__,

    license='',

    classifiers=[
        'Development Status :: 4 - Beta',

        'Intended Audience :: Science/Research',
        'Intended Audience :: Developers',

        'Topic :: Software Development',
        'Topic :: Scientific/Engineering',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],

    keywords='discrete 2D simulator',

    packages=find_packages(exclude=['docs', 'tests']),

    include_package_data=True,

    install_requires=['Flask', 'pyqt5'],

    python_requires='>=3',

    extras_require={
        'test': [
            'pytest',
            'coverage',
        ],
    },
)