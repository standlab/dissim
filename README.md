2D discrete surface simulator
=============================

Simulator of any rectangle discrete surface.

Install
-------
pip install -e git+https://gitlab.com/standlab/dissim.git#egg=dissim