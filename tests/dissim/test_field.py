# -*- coding: utf-8 -*-
"""
@author: Artem Vesnin
"""
from PyQt5.QtWidgets import QApplication

import sys
sys.path.append('../../')


from dissim.model import UnitModel
from dissim.model import FieldModel
from dissim.simulator import Field

unit1 = UnitModel(color = 'rgb(255, 100, 100)')
unit2 = UnitModel(color = 'rgb(100, 100, 100)')
unit3 = UnitModel(color = 'rgb(100, 255, 100)')
unit4 = UnitModel(color = 'rgb(100, 100, 255)')

field = FieldModel()
field.register_unit('unit1', unit1, x=0, y=0, rotation='up')
field.register_unit('unit2', unit2, x=0, y=-1, rotation='right')
field.register_unit('unit3', unit3, x=-1, y=-1, rotation='down')
field.register_unit('unit4', unit4, x=-1, y=0, rotation='left')


try:
    app = QApplication(sys.argv)
    field_gui = Field(field)
    sys.exit(app.exec_())

except BaseException:
    print('ERROR: something wrong')
    print('USAGE: python test_simulator.py [cols] [rows] [path]')
    raise
