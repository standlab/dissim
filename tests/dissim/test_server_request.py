# -*- coding: utf-8 -*-
"""
@author: Artem Vesnin
"""
import urllib.request
import time

url = 'http://127.0.0.1:5000/state'
print('Requesting ', url)
start = time.time()
content = urllib.request.urlopen(url).read()
print(content, '\nTook {}'.format(time.time() - start))
