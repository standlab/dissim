# -*- coding: utf-8 -*-
"""
@author: Artem Vesnin
"""
import os
import sys
sys.path.append('../../')

from dissim.icons import ICONS

for _, icon in ICONS.items():
    if not os.path.exists(os.path.join(os.getcwd(), '../../') + icon['path']):
        raise ValueError('Icon {} not found'.format(icon['path']))
print('Test OK')