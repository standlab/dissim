# -*- coding: utf-8 -*-
"""
@author: Artem Vesnin
"""

import tempfile
import sys
import os
sys.path.append('../../')

from PyQt5.QtWidgets import QApplication
from dissim.simulator import Field
from dissim.model import UnitModel
from dissim.model import FieldModel


try:
    unit = None
    if len(sys.argv) == 4:
        unit = UnitModel(dims=(int(sys.argv[1]), int(sys.argv[2])),
                               sol_pth=sys.argv[3])
    else:
        sol_file = os.path.join(tempfile.gettempdir(), 'simultor_state.json')
        unit = UnitModel(sol_pth=sol_file)


    field = FieldModel()
    field.register_unit('controls', unit, x=0, y=0, rotation='up')
    app = QApplication(sys.argv)
    simulator = Field(field)
    sys.exit(app.exec_())

except BaseException:
    print('ERROR: something wrong')
    print('USAGE: python test_simulator.py [cols] [rows] [path]')
    raise
