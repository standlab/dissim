# -*- coding: utf-8 -*-
"""
@author: Artem Vesnin
"""

import os
import sys
import tempfile
sys.path.append('../../')

from dissim.icons import ICONS
from PyQt5.QtWidgets import QApplication
from dissim.simulator import Field
from dissim.model import FieldModel
from dissim.model import Cell
from dissim.model import UnitModel

try:
    unit = None
    if len(sys.argv) == 4:
        unit = UnitModel(dims=(int(sys.argv[1]), int(sys.argv[2])),
                               sol_pth=sys.argv[3])
    else:
        sol_file = os.path.join(tempfile.gettempdir(), 'simultor_state.json')
        unit = UnitModel(sol_pth=sol_file)

    
    x = 0
    y = 0
    for icon_name, prop in ICONS.items():
        cell = Cell(icon_name, color='rgb(255,255,255)', icon=prop['path'])
        unit.register_cell(cell, 'main', x, y)
        x += 1
        if x >= unit.cols:
            y += 1
            x = 0
    field = FieldModel()
    field.register_unit('controls', unit, x=0, y=0, rotation='up')
    app = QApplication(sys.argv)
    simulator = Field(field)
    sys.exit(app.exec_())

except BaseException:
    print('ERROR: something wrong')
    print('USAGE: python test_simulator.py [cols] [rows] [path]')
    raise
